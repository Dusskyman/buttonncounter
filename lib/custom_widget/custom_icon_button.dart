import 'package:flutter/material.dart';

class CustomIconButton extends StatelessWidget {
  final Function onPress;
  final IconData iconData;
  final double size;

  CustomIconButton({
    this.iconData,
    this.onPress,
    this.size,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.blue,
      child: IconButton(
        highlightColor: Colors.white,
        icon: Icon(
          iconData,
          size: size,
          color: Colors.white,
        ),
        onPressed: onPress,
      ),
    );
  }
}
