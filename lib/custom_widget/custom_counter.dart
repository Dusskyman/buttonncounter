import 'package:flutter/material.dart';

class CustomCounter extends StatelessWidget {
  int counterData;
  CustomCounter({
    @required this.counterData,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
          color: Colors.grey[400], borderRadius: BorderRadius.circular(5)),
      child: Text(
        '${counterData}',
        style: TextStyle(fontSize: 30),
      ),
    );
  }
}
