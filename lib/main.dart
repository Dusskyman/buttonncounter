import 'dart:developer';

import 'package:counter_n_button/custom_widget/custom_counter.dart';
import 'package:counter_n_button/custom_widget/custom_icon_button.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MainScreen(),
    );
  }
}

class MainScreen extends StatefulWidget {
  int _counter = 0;
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CustomCounter(counterData: widget._counter),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomIconButton(
                iconData: Icons.arrow_upward,
                size: 30,
                onPress: () {
                  setState(() {
                    widget._counter++;
                  });
                },
              ),
              CustomIconButton(
                iconData: Icons.arrow_downward,
                size: 30,
                onPress: () {
                  setState(() {
                    widget._counter--;
                  });
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
